<?php

defined('MOODLE_INTERNAL') || die();
$messageproviders = array (
    // Notify teacher that a student has submitted a quiz attempt
    'submission' => array (
        'capability'  => 'local/feed:emailnotifysubmission'
    ),
    // Confirm a student's quiz attempt
    'confirmation' => array (
        'capability'  => 'local/feed:emailconfirmsubmission'
    )
);