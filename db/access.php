<?php

/**
 * @package    local_feed
 * @copyright  2017 Alex
 * @license    
 */

defined('MOODLE_INTERNAL') || die;

$capabilities = array(
    'local/feed:view' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'teacher' => CAP_ALLOW,
            'admin' => CAP_ALLOW,
        )
    ),

    'local/feed:addinstance' => array(
        'riskbitmask' => RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'admin' => CAP_ALLOW,
        )
    ),

);
