<?php

$observers  =  array ( 
    array ( 
        'eventname'  =>  '*' , 
        'callback'   =>  '\ tool_mylog \ log \ observer :: store' , 
        'internal'   =>  false ,  // Это означает, что мы получаем события только после транзакция commit. 
        'priority'   =>  1000 , 
    ) , 
) ;
