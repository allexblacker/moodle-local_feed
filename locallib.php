<?php

/**
 * @package    local_feed
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v4 or later
 * @author     Jan Eberhardt <eberhardt@math.tu-berlin.de>
 */

/**
 * Returns user setting or default 1
 *
 * @return number
 */
function local_feed_get_itemsnumber() {
	return intval(get_user_preferences('feed_itemsnumber', 0));
}

/**
 * Stores new user settings
 *
 * @param int $value
 */
function local_feed_update_itemsnumber($value) {
	$value = clean_param($value, PARAM_INT);
	set_user_preference('feed_itemsnumber', $value);
}