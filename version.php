<?php
 
/**
 * @package   local_feed
 * @copyright 2017, You Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_feed';
$plugin->version   = 2017121901;
$plugin->requires  = 2014121200;
$plugin->cron      = 0;
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = "1.0";