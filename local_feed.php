<?php

/**
 * @package    local_feed
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v4 or later
 * @author     Jan Eberhardt <eberhardt@math.tu-berlin.de>
 */

require_once(__DIR__ . "/locallib.php");

class local_feed extends block_base {
	
	public $items;
	
	public function init() {
		$this->title = get_string("pluginname", "local_feed");
	}
	
	public function instance_allow_config() {
		return true;
	}
	public function instance_allow_multiple() {
		return false;
	}
	public function applicable_formats() {
		return array("site-index" => true, "my-index" => true);
	}
	
	
	
	
}