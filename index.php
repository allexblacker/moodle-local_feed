<?php
/**
 * This page lists all the instances of lesson in a particular course
 *
 * @package local_feed
 * @copyright 2017 Alex
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/

/** Include required files */
require_once("../../config.php");
$a = optional_param('a', 0, PARAM_INT);
require_login();
require_capability('local/feed:view', context_system::instance());
$PAGE->set_url('/local/feed/index.php', array('a'=>$a));
$PAGE->set_pagelayout('standard');
$PAGE->set_context(context_system::instance());
$strlesson = get_string("pluginname", "local_feed");
$strlessons  = get_string("pluginnews", "local_feed");
$content = '';
if ($a>0) {
	$feed = $DB->get_record("feed", array("id" => $a));
	$title = $feed->title;
	$content .= "<div class=\"news_item clearfix\">";
	$content .= "<p class=\"new_text\">".$feed->description."</p>";
	$content .= "<div class=\"news_date pull-right text-muted\">".date("d.m.Y  H:i", strtotime($feed->date_add))."</div>";
	$content .= "</div>";
} else {
	$feed = $DB->get_records("feed", array('status'=>1), "id DESC");
	$title = $strlessons;
	foreach ($feed as $key => $f) {
		$content .= "<div class=\"news_item clearfix\">";
		$content .= "<h4><a href=\"/local/feed/index.php?a=".$f->id."\">".$f->title."</a></h4>";
		$content .= "<p class=\"new_text\">".$f->mini_desc."</p>";
		$content .= "<div class=\"news_date pull-right text-muted\">".date("d.m.Y  H:i", strtotime($f->date_add))."</div>";
		$content .= "</div>";
	}
	if (has_capability('local/feed:addinstance', context_system::instance())) {
		$content .= "<div class=\"clearfix\">";
		$content .= "<a class=\"btn btn-primary pull-right\" href=\"/local/feed/edit.php\">Добавить новость</a>";
		$content .= "</div>";
	}
	
}
if (!$feed) {
    //print_error('invalidcourseid');
}
/// Print the header
$PAGE->navbar->ignore_active();
if ($a>0) {
	$PAGE->navbar->add($strlessons, new moodle_url('/local/feed/index.php'));
	$PAGE->navbar->add($title);
} else {
	$PAGE->navbar->add($strlessons);
}
$PAGE->set_title($strlessons);
$PAGE->set_heading($strlesson);
echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2);

// вместо отдельного рендера
echo $content;

echo $OUTPUT->footer();
