<?php
/**
 * @package    local_feed
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v4 or later
 * @author     Alex
 */
require_once("../../config.php");
require_once("$CFG->libdir/formslib.php");
class feed_edit_form extends moodleform {
	
	// форма
	public function definition() {
		global $CFG, $DB, $OUTPUT;

		$mform =& $this->_form;
		$mform->addElement('text', 'title', 'Title', array('size'=>'64'));
		$mform->addElement('textarea', 'mini_desc', 'Mini description');
		$mform->addElement('textarea', 'description', 'Full description');
		$mform->addElement('submit', 'Save', 'Save');
	}
	
	// добавление в базу
	public function save($data=array()) {
		global $CFG, $DB;
		$res = false;
		if ($this->validate($data)) {
			$save = new stdClass();
			$save->title = $data['title'];
			$save->mini_desc = $data['mini_desc'];
			$save->description = $data['description'];
			$save->date_add = date("Y-m-d H:i:s");
			$save->status = 1;
			$res = $DB->insert_record('feed', $save, $returnid=true, $bulk=false);
		}
		return $res;
	}
	
	// простая валидация
	public function validate($data=array()) {
		$res = true;
		if (!isset($data['title'])||!$data['title']){
			$res = false;
		}
		if (!isset($data['mini_desc'])||!$data['mini_desc']){
			$res = false;
		}
		if (!isset($data['description'])||!$data['description']){
			$res = false;
		}
		unset($data['Save']);
		return $res;
	}
	
}