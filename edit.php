<?php
/**
 * This page lists all the instances of lesson in a particular course
 *
 * @package local_feed
 * @copyright 2017 Alex
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/

/** Include required files */
require_once("../../config.php");
require_once("edit_form.php");
$a = optional_param('a', 0, PARAM_INT);
require_login();
require_capability('local/feed:addinstance', context_system::instance());
$mform = new feed_edit_form();
if (isset($_POST)&&$_POST) {
	$mform->save($_POST);
	redirect('/local/feed/index.php');
} else {
	$PAGE->set_url('/local/feed/edit.php', array('a'=>$a));
	$PAGE->set_pagelayout('standard');
	$PAGE->set_context(context_system::instance());
	$strlesson = get_string("pluginname", "local_feed");
	$strlessons = get_string("pluginnews", "local_feed");
	$title = $strlessons;
	$PAGE->navbar->ignore_active();
	$PAGE->navbar->add($strlessons, new moodle_url('/local/feed/index.php'));
	if ($a>0) {
		$PAGE->navbar->add($title);
	} else {
		$PAGE->navbar->add("Добавить запись");
	}
	$PAGE->set_title($strlessons);
	$PAGE->set_heading($strlesson);
	echo $OUTPUT->header();
	echo $OUTPUT->heading($title, 2);
	// форма
	$mform->display();
	echo $OUTPUT->footer();
}

