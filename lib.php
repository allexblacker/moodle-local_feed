<?php

/**
 * @package local_feed
 * @copyright  2017 Alex
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

function local_feed_cron() {
	
}
function local_feed_extend_navigation(global_navigation $navigation) {
	if (has_capability('local/feed:view', context_system::instance())) {
		$newlink = navigation_node::create('Лента новостей', new moodle_url('/local/feed/index.php'), navigation_node::TYPE_CUSTOM);
		$newlink->showinflatnavigation = true;
		$navigation->add_node($newlink, 'mycourses');
	}
}
function local_feed_extend_settings_navigation() {
	
}
function local_feed_install() {
	
}