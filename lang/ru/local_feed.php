<?php

/**
 *
 * @package   local_feed
 * @copyright 2017
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Новостная лента';
$string['userpreferences'] = 'Пользовательские настройки';
$string['pluginnews'] = 'Новости';
$string['pluginempty'] = 'Сдесь будут новости';

$string['feed:view'] = 'view';
$string['feed:addinstance'] = 'addinstance';

$string['messageprovider:confirmation'] = 'Confirmation of your own quiz submissions';
$string['messageprovider:submission'] = 'Notification of quiz submissions';