<?php

/**
 *
 * @package   local_feed
 * @copyright 2017
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'News feed';
$string['userpreferences'] = 'User preferences';
$string['pluginnews'] = 'News';
$string['pluginempty'] = 'News will be here';

$string['feed:view'] = 'view';
$string['feed:addinstance'] = 'addinstance';

$string['messageprovider:confirmation'] = 'Confirmation of your own quiz submissions';
$string['messageprovider:submission'] = 'Notification of quiz submissions';